fixed
=====

*fixed* package implements fast fixed-point arithmetics for Go.

## Installation

    go get github.com/fzzy/fixed/fixed


## Documentation and examples

Documentation is available at http://godoc.org/github.com/fzzy/fixed/fixed.

Alternatively, run godoc:

	godoc -http=:8080

and point your browser to http://localhost:8080/pkg/github.com/fzzy/fixed/fixed.


## HACKING

If you make contributions to the project, please follow the guidelines below:

*  Maximum line-width is 100 characters.
*  Run "gofmt -w -s" for all Go code before pushing your code. 


## Copyright and licensing

*Copyright 2013 Juhani Åhman*. 
Unless otherwise noted, the source files are distributed under the
*MIT License* found in the LICENSE file.
