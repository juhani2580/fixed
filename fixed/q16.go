package fixed

type Q16 int32

const (
	Q16Max Q16 = 0x7FFFFFFF  // Maximum value of Q16 (~ 32768.49999)
	Q16Min Q16 = -0x80000000 // Minimum value of Q16 (~ -32768.50000)
	q16One  Q16 = 1 << 16
)

//* Conversion functions

// Itoq16 converts the given integer to Q16.
func Itoq16(a int) Q16 {
	return Q16(a) * q16One
}

// F32toq16 converts the given float32 to Q16.
func F32toq16(a float32) Q16 {
	return Q16(a * float32(q16One))
}

// F64toq16 converts the given float64 to Q16.
func F64toq16(a float64) Q16 {
	return Q16(a * float64(q16One))
}

//* Methods

// Int converts the Q16 to int.
func (a Q16) Int() int {
	return int(a / q16One)
}

// Float32 converts the Q16 to float32.
func (a Q16) Float32() float32 {
	return float32(a) / 0xFFFF
}

// Float64 converts the Q16 to float64.
func (a Q16) Float64() float64 {
	return float64(a) / 0xFFFF
}

func (a Q16) Add(b Q16) Q16 {
	return a + b
}

func (a Q16) Sub(b Q16) Q16 {
	return a - b
}

func (a Q16) Mod(b Q16) Q16 {
	return a%b
}

func (a Q16) Mul(b Q16) Q16 {
	// Q16.16*Q16.16 -> Q32.32 multiplication
	x := int64(a) * int64(b)
	// correct to Q16.16
	x >>= 16
	return Q16(x)
}

func (a Q16) Div(b Q16) Q16 {
	// Upscale to Q32.32 so the result will be in Q16.16
	x := int64(a) << 16
	x = x / int64(b)
	return Q16(x)
}
