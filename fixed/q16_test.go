package fixed

import (
	. "launchpad.net/gocheck"
)

type Q16Suite struct{}

var _ = Suite(&Q16Suite{})

func (s *Q16Suite) TestItoq16(c *C) {
	c.Check(Itoq16(2), Equals, Q16(0x20000))
}

func (s *Q16Suite) TestF32toq16(c *C) {
	c.Check(F32toq16(2.0), Equals, Q16(0x20000))
}

func (s *Q16Suite) TestF64toq16(c *C) {
	c.Check(F64toq16(2.0), Equals, Q16(0x20000))
}

func (s *Q16Suite) TestInt(c *C) {
	c.Check(q16One.Int(), Equals, 1)
}

func (s *Q16Suite) TestFloat32(c *C) {
	q := F64toq16(12.34).Float32()
	if 12.33 > q || q > 12.35 {
		c.Fail()
	}
}

func (s *Q16Suite) TestFloat64(c *C) {
	q := F64toq16(12.34).Float64()
	if 12.33 > q || q > 12.35 {
		c.Fail()
	}
}

func (s *Q16Suite) TestAdd(c *C) {
	c.Check(Itoq16(1).Add(Itoq16(2)).Int(), Equals, 3)
}

func (s *Q16Suite) TestSub(c *C) {
	c.Check(Itoq16(3).Sub(Itoq16(2)).Int(), Equals, 1)
}

func (s *Q16Suite) TestMod(c *C) {
	c.Check(Itoq16(5).Mod(Itoq16(2)).Int(), Equals, 1)
}

func (s *Q16Suite) TestMul(c *C) {
	c.Check(Itoq16(2).Mul(Itoq16(3)).Int(), Equals, 6)
	c.Check(Itoq16(-2).Mul(Itoq16(3)).Int(), Equals, -6)
	c.Check(Itoq16(-2).Mul(Itoq16(-3)).Int(), Equals, 6)
	c.Check(Itoq16(2).Mul(Itoq16(-3)).Int(), Equals, -6)
}

func (s *Q16Suite) TestDiv(c *C) {
	c.Check(Itoq16(9).Div(Itoq16(3)).Int(), Equals, 3)
	c.Check(Itoq16(-9).Div(Itoq16(3)).Int(), Equals, -3)
	c.Check(Itoq16(-9).Div(Itoq16(-3)).Int(), Equals, 3)
}

//* Benchmarks

func (s *Q16Suite) BenchmarkAdd(c *C) {
	q := F64toq16(3.14)
	for i := 0; i < c.N; i++ {
		q.Add(q)
	}
}

func (s *Q16Suite) BenchmarkSub(c *C) {
	q := F64toq16(3.14)
	for i := 0; i < c.N; i++ {
		q.Sub(q)
	}
}

func (s *Q16Suite) BenchmarkMul(c *C) {
	q := F64toq16(3.14)
	for i := 0; i < c.N; i++ {
		q.Mul(q)
	}
}

func (s *Q16Suite) BenchmarkDiv(c *C) {
	q1 := F64toq16(3.14)
	q2 := F64toq16(5.12)
	for i := 0; i < c.N; i++ {
		q1.Div(q2)
	}
}
